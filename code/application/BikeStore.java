package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bicycle = new Bicycle[4];
        bicycle[0] = new Bicycle("Minelli", 26, 22);
        bicycle[1] = new Bicycle("Giant", 28, 41);
        bicycle [2] = new Bicycle("Trek", 21, 30);
        bicycle[3] = new Bicycle("SuperCycle", 24, 31);

        for(Bicycle bikes : bicycle){
            System.out.println(bikes);
        }
    }
}
